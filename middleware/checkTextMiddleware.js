const Joi = require('joi')

const checkText = async (req, res, next) => {
    const schema = Joi.object({
        text: Joi.string()
            .max(60)
            .required()
            .trim(false)
    })
    try {
        await schema.validateAsync(req.text)
        next()
    } catch (error) {
        res.status(400).json({message: 'Text must contain more than 3 and less than 60 characters'})
    }
}

module.exports = {checkText}