const Joi = require('joi')

const checkCredentials = async (req, res, next) => {
    const schema = Joi.object({
        username: Joi.string()
            .alphanum()
            .max(30)
            .required(),
        password: Joi.string()
            .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    })
    try {
        await schema.validateAsync(req.body)
        next()
    } catch (error) {
        res.status(400).json({message: 'Invalid input data'})
    }
}

module.exports = {checkCredentials}