const {Router} = require('express')
const {login, register} = require('../controllers/authControler')
const {checkCredentials} = require('../middleware/authMiddleware')
const router = Router()

router.post('/login',login)
router.post('/register', checkCredentials, register)

module.exports = router 