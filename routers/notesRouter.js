const {Router} = require('express')
const {getNotes, getNote, addNote, updateNote, checkNote , deleteNote} = require('../controllers/notesControler')
const { tokenMiddleware } = require('../middleware/tokenMiddleware')
const { checkText } = require('../middleware/checkTextMiddleware')
const router = Router()

router.get('/',tokenMiddleware, getNotes)
router.post('/', [tokenMiddleware, checkText], addNote)
router.get('/:id', tokenMiddleware,getNote)
router.put('/:id',tokenMiddleware, updateNote)
router.patch('/:id',tokenMiddleware, checkNote)
router.delete('/:id',tokenMiddleware, deleteNote)

module.exports = router 