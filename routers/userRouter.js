const {Router} = require('express')
const {getProfileInfo, deleteProfile, changePassword} = require('../controllers/userControler')
const { tokenMiddleware } = require('../middleware/tokenMiddleware')
const {checkOldPassword, checkNewPassword } = require('../middleware/changePasswordMiddleware')
const router = Router()

router.get('/', tokenMiddleware, getProfileInfo)
router.delete('/', tokenMiddleware, deleteProfile)
router.patch('/', [tokenMiddleware, checkOldPassword, checkNewPassword], changePassword)

module.exports = router  