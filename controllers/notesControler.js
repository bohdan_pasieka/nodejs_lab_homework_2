const Note = require('../models/notesModel')

const getNotes = async (req, res) => {
    const {_id} = req.user
    const {offset = 0, limit = 5} = req.query
    const note = await Note.find({userId: _id}, {__v: 0}).skip(+offset).limit(+limit)

    if (!note) {
        res.status(400).json({message: "Can not find note"})
        return
    }
     res.status(200).json({notes: note})      
} 

const addNote = async (req, res) => {
    const {text} = req.body
    const {_id} = req.user 
    const note = new Note({ 
            userId: _id,
            text
    })
    await note.save()
    res.status(200).json({message: 'Note has been added'})
}

const getNote = async(req, res) => {
    const {id} = req.params
    const note = await Note.findById(id, {__v: 0}, (err) => {
        if (err) {
            res.status(400).json({message: "There is no one note"})
            return
        }
    })

    if (!note) {
        res.status(400).json({message: "There is no one note"})
        return
    }

    res.status(200).json({note})
}

const updateNote = async(req, res) => {
    const {id} = req.params
    const {text} = req.body

    await Note.findByIdAndUpdate(id, {text}, (err) => {
        if (err) {
            res.status(400).json({message: 'Server error'})
        }
    })

    res.status(200).json({message: 'Note has been updated'})
}

const checkNote = async (req, res) => {
    const {id} = req.params
    const note = await Note.findById(id, (err) => {
        if (err) {
            res.status(400).json({message: 'Error'})
            return
        }
    })
    if (!note) {
        res.status(400).json({message: 'Error'})
        return
    }
    await Note.findByIdAndUpdate(id, {completed: !note.completed}, (err)=> {
        if (err) {
            res.status(400).json({message: 'Error'})
            return
        }
    })
    res.status(200).json({message: 'Note is completed'})
} 

const deleteNote = async(req, res) => {
    const {id} = req.params
    const note = await Note.findById(id, (err) => {
        if (err) {
            res.status(400).json({message: 'Error'})
            return
        }
    })
    if (!note) {
        res.status(400).json({message: 'Error'})
        return
    }
    await Note.findByIdAndDelete(note._id, (err) => {
        if (err) {
            res.status(400).json({message: 'Error'})
            return
        }
    })
    res.status(200).json({message: 'Note is just deleted'})
}

module.exports = {getNotes, getNote, addNote, updateNote, checkNote , deleteNote}