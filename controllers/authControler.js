const User = require('../models/authModel')
const bcrypt = require('bcrypt')
const jwt = require ('jsonwebtoken')
const {JWT_SECRET} = require('../config')

const login = async (req, res) => {
    const {username, password} = req.body
    const user = await User.findOne({username}, (err) => {
        if (err) {
            res.status(400).json({message: 'User was not found'})
            return
        }
    }) 
    if (!user) {
        res.status(400).json({message: 'User was not found'})
        return
    }

    if (!(await bcrypt.compare(password, user.password))) {
        res.status(400).json({message: 'Wrong password'})
    }

    const token = jwt.sign({ username: user.username, _id: user._id }, JWT_SECRET)
    res.status(200).json({message: 'You has signed in', jwt_token: token})
}

const register = async (req, res) => {
    const {username, password} = req.body
   
    const user = await User.findOne({username: username})
    if (!user) {
        const cryptPassword = await bcrypt.hash(password, 10)
        const newUser = new User({username, password: cryptPassword})
        await newUser.save()

        res.status(200).json({message: 'User has been created'})    
    } else {
        res.status(400).json({message: 'User with this username is already created'})  
    }
} 

module.exports = {login, register} 