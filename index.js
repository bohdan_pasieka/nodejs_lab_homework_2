const express = require('express')
const mongoose = require('mongoose')
const authRouter = require('./routers/authRouter.js')
const notesRouter = require('./routers/notesRouter.js')
const userRouter = require('./routers/userRouter')

const app = express()
const PORT  = process.env.PORT || 8080
app.use(express.json())
app.use('/api/auth', authRouter)
app.use('/api/notes', notesRouter)
app.use('/api/users/me', userRouter)

const start = async() => {
    await mongoose.connect('mongodb+srv://bohdan:1234@cluster0.71u5o.mongodb.net/Users?retryWrites=true&w=majority', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true
    })
    app.listen(PORT, () => {console.log(`Server has been launched at port ${PORT}`)})
}
start()
