const {Schema, model} = require('mongoose')

const user = new Schema({
        userId: {
            type: Schema.Types.ObjectId,
            required: true, 
            ref: 'User'
        },
        completed: {
            type: Boolean,
            default: false
        },
        text: {
            type: String,
            required: true
        },
        createdDate: {
            type: Date,
            default: Date.now()
        }
})


module.exports = model('Note', user, 'notes')